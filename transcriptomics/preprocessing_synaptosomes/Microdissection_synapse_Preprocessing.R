# pre-processing
# R Script Title: RNA-seq processing of HC samples for GAD2 samples
# Author: Eva Kaulich and then edited by Georgi Tuchev
# Date: November 18, 2023
# Description: This script performs data import, preprocessing on synaptosome RNA-seq data

library(rstudioapi)


# --- function readDataTable --- #
# assign sample name from barcodes whitelist
readDataTable <- function(file_name, whitelist, tag) {
  data <- read.table(file_name, header = TRUE, row.names = 1, sep = "\t")
  idx_sample <- match(whitelist$barcodes, colnames(data))
  print(idx_sample)
  data <- data[, idx_sample]
  if (all(colnames(data) == whitelist$barcodes)) {
    colnames(data) <- paste(whitelist$samples, tag, sep = "_")
  }
  return(data)
}


# --- set working directory --- #
setwd(dirname(rstudioapi::getActiveDocumentContext()$path))

# --- read whitelist --- #
whitelist <- read.table("../counts/whitelist.csv", header = T, sep = ';')

# --- read count files --- #
filelist <- read.table("../counts/filelist_HCMicrodissections.csv", header = T, sep = ';') # fro some reson if it's not seperated by ; it doesn't work for this file idk why

list_colnames <- character(0)
data_list <- list()
for(f in 1:nrow(filelist)) {
  data <- readDataTable(filelist$filename[f], whitelist, filelist$tag[f])
  data_list[[f]] <- data
  list_colnames <- c(list_colnames, colnames(data))
}

# Get unique row names from all matrices
list_colnames <- sort(list_colnames)
list_rownames <- unique(unlist(lapply(data_list, rownames)))

num_rows <- length(list_rownames)
num_cols <- length(list_colnames)

# Create an empty matrix with the correct dimensions
data_merged <- matrix(0, nrow = num_rows, ncol = num_cols)
rownames(data_merged) <- list_rownames
colnames(data_merged) <- list_colnames

for (k in 1:length(data_list)) {
  data <- data_list[[k]]
  idx_rows <- match(rownames(data), list_rownames)
  idx_cols <- match(colnames(data), list_colnames)
  data_merged[idx_rows, idx_cols] <- as.matrix(data)
}


write.table(data_merged,
            file = "../counts/tableCounts_HCMicrodissections_syn_temp.txt",
            sep = "\t",
            quote = F,
            row.names=T,
            col.names=T)
