# HippocampusMicrodissections
This code can be cited using the following DOI
https://doi.org/10.17617/1.1WVD-V337

The following project contains the code and bioinformatics pipeline used in the following preprint, version 1: Kaulich, E.* , Waselenchuk, Q.*, Fuerst, N., Desch, K., Mosbacher, J., Ciirdaeva, E., Juengling, M., Tushev, G, Langer, J., Schuman E.M. The molecular diversity of hippocampal regions and strata at synaptic resolution revealed by integrated transcriptomic and proteomic profiling. bioRxiv (2024). doi:10.1101/2024.08.05.606570. *contributed equally


## Sample Description

The project includes RNA-seq and LC-MS/MS analyses of microdissected samples. These analyses are performed on two types of samples: tissue and synaptosomes.
- [ ] Hippocampal regions (CA1, CA3, DG)
- [ ] CA1 strata (_S.oriens, S.pyramidale, S.radiatum, S.lacunosum-moleculare_)

The RNA-seq data can be accessed at [NCBI SRA](https://www.ncbi.nlm.nih.gov/sra) with the following accession number: PRJNA1142450 (tissue) and PRJNA1142761 (synaptosomes).

The LC-MS/MS data will be available soon with the following accession number: 


## Authors and acknowledgment
We thank all the authors who contributed to the code: Kaulich, E., Waselenchuk, Q., Desch, K., Mosbacher, J., Juengling, M., Tushev, G. 

