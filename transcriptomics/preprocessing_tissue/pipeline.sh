#!/bin/bash

#SBATCH --nodes=1
#SBATCH --partition=cuttlefish
#SBATCH --time=100:00:00
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=32
#SBATCH --job-name=mmRNA
#SBATCH --error=error_%j.txt
#SBATCH --output=output_%j.txt

echo $SLURM_SUBMIT_DIR
echo "Running on `hostname`"

# configure environment
bcl2fastq=/gpfs/scic/software/biotools/bcl2fastq2/bin/bcl2fastq
fastp=/gpfs/scic/software/biotools/fastp/fastp
star=/gpfs/scic/software/biotools/STAR-2.7.10a/build/STAR
samtools=/gpfs/scic/software/biotools/htslib/bin/samtools
featurecounts=/gpfs/scic/software/biotools/subread-2.0.3/bin/featureCounts
genome_index=/gpfs/scic/data/genomes/Mouse/mm39/stardb
genome_ann=/gpfs/scic/data/genomes/Mouse/mm39/mm39_ncbiRefSeq_exons_09Jun2021.gtf

# configure run & project
path_run=/gpfs/scic/ilmndata/runs/2023/231117_VH00412_96_AAFC2M7M5
run_id=231117_96_AAFC2M7M5
path_project=/gpfs/schu/data/Synapse_Diversity/HippocampusMicrodissections/Tissue/Transcriptomics
sample_sheet=${path_project}/samplesheet/SampleSheet_IEM_EvaK_mRNAseq_20231117.csv
adapter_1=AGATCGGAAGAGCACACGTCTGAACTCCAGTCA
adapter_2=AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGT
adapter_fa=${path_project}/samplesheet/indexes.fa

# configure pipeline paths
if [ ! -d "$path_project" ]; then
    echo "error, please create valid project folder."
    exit 1
fi
path_fastq=${path_project}/fastq
path_fastq_clean=${path_fastq}/clean
path_bams=${path_project}/bams
path_counts=${path_project}/counts

# demultiplexing
if [ ! -d "$path_fastq" ]; then
    echo "BCL2FASTQ demultiplexing ..."
    mkdir -p "$path_fastq"
    
    $bcl2fastq \
    --runfolder-dir $path_run \
    --sample-sheet $sample_sheet \
    --output-dir $path_fastq \
    --loading-threads 8 \
    --processing-threads 8 \
    --writing-threads 8 \
    --no-lane-splitting
fi



# clean
path_fastq_clean="$path_fastq"
if [ ! -d "$path_fastq_clean" ]; then
    echo "FASTP cleaning ..."
    mkdir -p "$path_fastq_clean"
    
    for file_fq_r1 in ${path_fastq}/*_R1_*.fastq.gz
    do
        file_name=$(basename "$file_fq_r1")
        sample_name="${file_name%%_S[0-9]*_R[0-9]*_001.fastq.gz}"
        [[ "$sample_name" =~ ^Undetermined* ]] && continue
        file_fq_r2=$(ls ${path_fastq}/${sample_name}*_R2_*.fastq.gz)
    
        echo $sample_name

        $fastp \
        --in1=$file_fq_r1 \
        --in2=$file_fq_r2 \
        --out1=$path_fastq_clean/${sample_name}_R1_clean.fastq.gz \
        --out2=$path_fastq_clean/${sample_name}_R2_clean.fastq.gz \
        --compression=4 \
        --adapter_sequence=$adapter_1 \
        --adapter_sequence_r2=$adapter_2 \
        --adapter_fasta=$adapter_fa \
        --trim_poly_x \
        --poly_x_min_len 10 \
        --average_qual=20 \
        --reads_to_process=0 \
        --thread=16 \
        --json=$path_fastq_clean/${sample_name}_fastp.json \
        --html=$path_fastq_clean/${sample_name}_fastp.html \
        2> $path_fastq_clean/${sample_name}_fastp.txt
        
#        --low_complexity_filter \
#        --complexity_threshold=30 \
#        --length_required=21 \
#        --cut_tail \
#        --cut_tail_window_size=4 \
#        --cut_tail_mean_quality=20 \
#        --cut_front \
#        --cut_front_window_size=4 \
#        --cut_front_mean_quality=20 \
        
        
    done

fi


# alignment
if [ ! -d "$path_bams" ]; then
    echo "STAR alignment ..."
    mkdir -p "$path_bams"

    for file_fq_r1 in $path_fastq_clean/*_R1_001.fastq.gz
    do
        sample_name=$(basename $file_fq_r1 "_R1_001.fastq.gz")
        file_fq_r2=$(ls $path_fastq_clean/${sample_name}_R2_001.fastq.gz)
        [[ "$sample_name" =~ ^Undetermined* ]] && continue

        echo $sample_name
        
        $star \
        --runMode alignReads \
        --runThreadN 16 \
        --genomeDir $genome_index \
        --readFilesIn $file_fq_r1 $file_fq_r2 \
        --readFilesCommand zcat \
        --outSAMmultNmax 1 \
        --outSAMattributes All \
        --outSAMstrandField intronMotif \
        --outFilterIntronMotifs RemoveNoncanonical \
        --outSAMtype BAM SortedByCoordinate

        mv Aligned.sortedByCoord.out.bam ${path_bams}/${sample_name}.bam
        mv Log.final.out ${path_bams}/${sample_name}_log.txt
        mv SJ.out.tab ${path_bams}/${sample_name}_SJ.txt
        rm -f *.out
    
        $samtools index ${path_bams}/${sample_name}.bam
    done

fi

# summary alignment
if [ ! -f ${path_bams}/summary_alignment.txt ]; then
    echo "Summary alignment ..."
    echo "sample"$'\t'"reads.raw"$'\t'"reads.unique" > ${path_bams}/summary_alignment.txt
    grep -H -e "Number of input reads" -e "Uniquely mapped reads number" ${path_bams}/*_log.txt | \
    awk -F" " 'OFS="\t"{gsub("_log.txt:","",$1);N=split($1,TAG,"/");if(NR%2==0){Lunq[TAG[N]]=$(NF)}else{Lraw[TAG[N]]=$(NF)}}END{for(key in Lraw) print key,Lraw[key],Lunq[key]}' | \
    sort -k1,1 >> ${path_bams}/summary_alignment.txt
fi


# counting
if [ ! -d "$path_counts" ]; then
    echo "FeatureCounts counting"
    mkdir -p "$path_counts"

    $featurecounts \
    -t exon \
    -g gene_id \
    -Q 255 \
    -p --countReadPairs \
    -T 16 \
    -a $genome_ann \
    -o ${path_counts}/tableCounts_${run_id}.txt \
    $(ls ${path_bams}/*.bam)

fi

echo "all done."
