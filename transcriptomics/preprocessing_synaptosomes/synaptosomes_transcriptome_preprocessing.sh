#!/bin/bash
#Computational pipeline for the conversion of illumina bcl-file to DGE-matrices based on the Dropseq core pipeline from the Mc Carroll lab (github: https://github.com/broadinstitute/Drop-seq/releases; publication: https://doi.org/10.1016/j.cell.2015.05.002)
#Pipeline was initialized by Dr. Julio Perez in the lab of Erin Schuman and updated as well as modified for the project by Janus Mosbacher 
#### Convert Illumina run files (base call files) to FASTQ format for downstream processing using bcl2fastq###
"
echo Demultiplex #this step converts the raw sequencing data into Fastq format and splits them according to index used in the Nextera PCR

/gpfs/scic/software/biotools/bcl2fastq2/bin/bcl2fastq --runfolder-dir /gpfs/scic/ilmndata/runs/2023/231211_VH00412_106_AAFC2T5M5 \
 --output-dir /gpfs/schu/data/Synapse_Diversity/HippocampusMicrodissections/Synaptosomes/Transcriptomics  \
 --no-lane-splitting --loading-threads 8 --writing-threads 8 --minimum-trimmed-read-length 0 --mask-short-adapter-reads 0 \
 --sample-sheet /gpfs/schu/data/Synapse_Diversity/HippocampusMicrodissections/Synaptosomes/Transcriptomics/EvaK_HipMicroDis_SampleSheet.csv

mkdir DeMultiplexed_Fastqs
mv Stats/ DeMultiplexed_Fastqs/
mv Reports/ DeMultiplexed_Fastqs/
### END ###


# ### CALCULATE QUALITY METRICS USING FastQC ###
echo FastqQC

mkdir DeMultiplexed_Fastqs/FastQC

/gpfs/scic/software/biotools/FastQC/fastqc *.fastq.gz
mv *fastqc* DeMultiplexed_Fastqs/FastQC/
mv Undetermined* DeMultiplexed_Fastqs/
### END ###
##########################################################################################################################
echo FastqToSam #convert Fastq to Sam format and matches

for l in *R1_001.fastq.gz;

do
Exp=$(echo "$l"|cut -d "_" -f1)
number=$(echo "$l"|cut -d "_" -f2)
#sample=$(echo "$l"|cut -d "_" -f3)
echo $Exp"_"$number"_"$sample"_R2_001.fastq.gz"

java -Xmx16G -jar /gpfs/scic/software/biotools/picard/build/libs/picard.jar FastqToSam \
        F1=${l} \
        F2=$Exp"_"$number"_R2_001.fastq.gz" \
        O=$Exp"_"$number".bam" \
        SM=$Exp"_"$number
done

mv *fastq.gz DeMultiplexed_Fastqs



###########################################################################################################################
echo ExtractBarcodes #extracts index and UMI sequence from Read 1
mkdir Summaries
mkdir Summaries/Indexing_Summaries
mkdir Summaries/UMIing_Summaries

for c in *.bam;
do
/gpfs/scic/software/biotools/drop-seq-tools/TagBamWithReadSequenceExtended \
INPUT=${c} \
OUTPUT=Indexed_${c} \
SUMMARY=Indexed_${c}_summary.txt \
BASE_RANGE=11-18 \
BASE_QUALITY=10 \
BARCODED_READ=1 \
DISCARD_READ=False \
TAG_NAME=XC \
NUM_BASES_BELOW_QUALITY=1

/gpfs/scic/software/biotools/drop-seq-tools/TagBamWithReadSequenceExtended \
INPUT=Indexed_${c} \
OUTPUT=UMIed_Indexed_${c} \
SUMMARY=UMIed_Indexed_${c}_summary.txt \
BASE_RANGE=1-10 \
BASE_QUALITY=10 \
BARCODED_READ=1 \
DISCARD_READ=True \
TAG_NAME=XM \
NUM_BASES_BELOW_QUALITY=1

rm ${c}
rm Indexed_${c}
mv Indexed_${c}_summary.txt Summaries/Indexing_Summaries
mv UMIed_Indexed_${c}_summary.txt Summaries/UMIing_Summaries
done
############################################################################################################################
echo FilterBam #Remove reads with low quality index or UMI
mkdir UMIed_Bams

for d in *.bam;

do
/gpfs/scic/software/biotools/drop-seq-tools//FilterBam \
      TAG_REJECT=XQ \
      INPUT=${d} \
      OUTPUT=Filtered_${d}

mv ${d} UMIed_Bams
done

#############################################################################################################################
echo TrimSMART #Remove parts of reads where the template switch oligo was sequenced
mkdir Summaries/SMART_Trimmed_Summaries

for e in *.bam;

do
  /gpfs/scic/software/biotools/drop-seq-tools/TrimStartingSequence \
  INPUT=${e} \
  OUTPUT=SMART_Trimmed_${e} \
  OUTPUT_SUMMARY=SMART_Trimmed_${e}_summary.txt \
  SEQUENCE=AAGCAGTGGTATCAACGCAGAGTGTCGTGACTGGGAAAACCCTGGCGGG \
  MISMATCHES=0 \
  NUM_BASES=5

rm ${e}
mv SMART_Trimmed_${e}_summary.txt Summaries/SMART_Trimmed_Summaries
done

#############################################################################################################################
echo TrimPolyA #Remove parts of reads where the polyA tail was sequenced
mkdir Summaries/PolyA_Trimmed_Summaries

for f in *.bam;

do
/gpfs/scic/software/biotools/drop-seq-tools/PolyATrimmer \
INPUT=${f} \
OUTPUT=PolyA_Trimmed_${f} \
OUTPUT_SUMMARY=PolyA_Trimmed_${f}_summary.txt \
MISMATCHES=0 \
NUM_BASES=6 \
USE_NEW_TRIMMER=true

rm ${f}
mv PolyA_Trimmed_${f}_summary.txt Summaries/PolyA_Trimmed_Summaries
done

############################################################################################################################
echo SamToFastq #Convert back file from Sam to Fastq format

for g in *.bam;

do
java -Xmx16G -jar /gpfs/scic/software/biotools/picard/build/libs/picard.jar SamToFastq \
INPUT=${g} \
FASTQ=${g}.fastq
rm ${g}
done

#############################################################################################################################
echo FinalQualityFiltering #Remove reads of low complexity reads or containing homopolymers

mkdir Fastp_Files
##################
for g in *.fastq;

do
/gpfs/scic/software/biotools/fastp/fastp -y -x -i ${g} -o QCed_${g}
mv fastp.html ${g}.html
mv fastp.json ${g}.json
rm ${g}
done

mv *html Fastp_Files/
mv *json Fastp_Files/

#############################################################################################################################
echo STAR_Alignment #Align reads to the mouse genome
mkdir Alignment_Logs/
mkdir Filtered_Fastq/

for h in *.fastq;
do
 /gpfs/scic/software/biotools/STAR-2.7.10a/source/STAR --runMode alignReads --runThreadN 8 \
 --genomeDir /gpfs/schu/data/RNA/Projects/synapse_states/2023/Analysis/Janus/GRCm39_Jun2023/STAR \
 --outReadsUnmapped Fastx --readFilesIn ${h} --outStd Log --outSAMtype BAM SortedByCoordinate --alignSoftClipAtReferenceEnds No \
 --outFilterScoreMinOverLread 0.66 --outFilterMatchNminOverLread 0.66;
  mv Aligned.sortedByCoord.out.bam ${h}.bam;
  mv Unmapped.out.mate1 ${h}.unmapped.fasta;
  mv Log.final.out ${h}.log.final.out;
  mv Log.out ${h}.log.out;
  rm Log.progress.out
  rm -R _STARtmp
mv ${h} Filtered_Fastq/
mv ${h}.log* Alignment_Logs/
done

rm SJ.out.tab


############################################################################################################################
echo MergeBams #Recombined aligned reads 2 to their UMI and index reads 1

mkdir Aligned_Bams
mv UMIed_Bams/*.bam .

for l in UMIed_Indexed_*;

do
java -Xmx16G -jar /gpfs/scic/software/biotools/picard/build/libs/picard.jar MergeBamAlignment \
REFERENCE_SEQUENCE= /gpfs/schu/data/RNA/Projects/synapse_states/2023/Analysis/Janus/GRCm39_Jun2023/fasta/GRCm39.genome.fa \
UNMAPPED_BAM=${l} \
ALIGNED_BAM=QCed_PolyA_Trimmed_SMART_Trimmed_Filtered_${l}.fastq.bam \
OUTPUT=Merged_PolyA_Trimmed_SMART_Trimmed_Filtered_${l} \
INCLUDE_SECONDARY_ALIGNMENTS=false \
PAIRED_RUN=false \

mv ${l} UMIed_Bams
mv QCed_PolyA_Trimmed_SMART_Trimmed_Filtered_${l}.fastq.bam Aligned_Bams
done

#############################################################################################################################
echo TagReads #Assign reads to gene models

mkdir Summaries/Exon_Tags_Summaries/

for j in *.bam;

do
/gpfs/scic/software/biotools/drop-seq-tools/TagReadWithGeneFunction \
I=${j} \
O=Exon_Tagged_${j} \
ANNOTATIONS_FILE=/gpfs/schu/data/RNA/Projects/synapse_states/2023/Analysis/Janus/GRCm39_Jun2023/genes/GRCm39_M33_gencode.refFlat\

rm ${j}
mv *.summary.txt Summaries/Exon_Tags_Summaries/
done

############################################################################################################################
echo DigitalExpression #Count reads per gene

cp /gpfs/schu/data/RNA/Projects/Single_dendrite_RNA-seq/Tools/Barcodes_16.txt .
mkdir Bams_Exon_Tagged/

for k in *.bam;

do
/gpfs/scic/software/biotools/drop-seq-tools/DigitalExpression \
I=${k} \
O=${k}.DGE.txt.gz \
STRAND_STRATEGY=SENSE \
SUMMARY=${k}.DGE.txt.gz.summary.txt \
CELL_BC_FILE=Barcodes_16.txt \
TMP_DIR=Temp

mv ${k} Bams_Exon_Tagged/
mv *summary* Summaries/
done

rm Barcodes_*.txt
